# README #

### What is this repository for? ###

* This repository is to connect to REST API endpoints 
* To solve the challenges


### How do I get set up? ###

* this code is written in java which requires jdk1.8+
* clone this repo
* cd to the root directory 
* run `mvn -q test`