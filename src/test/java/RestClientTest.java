package test.java;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.testng.annotations.Test;

import com.google.gson.Gson;

import java.util.ArrayList;
public class RestClientTest {
	
	private static List<Trader> allTradersList;
	private static List<Transactions> allTransactionsList;
	
	
		@Test
		public static void main() {
			String tradersJson = getTradersJson();
			String transJson = getTransactionsJson();
			System.out.println("\n\n\n");
//			System.out.println(transJson);
			Transactions[] transA = new Gson().fromJson(transJson, Transactions[].class);
			Trader[] traderA = new Gson().fromJson(tradersJson, Trader[].class);
			allTransactionsList = Arrays.asList(transA);
			allTradersList = Arrays.asList(traderA);
			
	
			
			BiFunction<String, List<Trader>, List<Trader>> tradersByCity=
					(city,traderList) ->
										traderList.stream()
										.filter(trader->trader.getCity().equals(city))
										.collect(Collectors.toList()
										);	
									
			Function<List<Trader>, List<String>> tradersById =
					traderList ->
										traderList.stream()										
										.map(trader->trader.getId())
										.collect(Collectors.toList());	
									
			BiFunction<String,List<Transactions>, Double> avgTransactionsByCity = 
					(city,txnsList) -> txnsList.stream()
														.filter(
																  txn-> (
																		  tradersByCity.andThen(tradersById).apply(city, allTradersList)
																			.contains(txn.getTraderId()))
															          )
														.mapToDouble(e->e.getValue())
														.average()
														.getAsDouble();
										
			Function<List<Trader>, List<String>> tradersSortByName =
					tradersList ->
									tradersList.stream()										
										.map(trader->trader.getName())
										.sorted()
										.collect(Collectors.toList());	
									
			Function<List<Double>, Double> first =  
										    a -> a.stream().findFirst().get();
									
									
			Function<List<Transactions>,List<Double>> txnSortByValueDesc = 
					txnList -> txnList
												.stream()
												.map(txn-> txn.getValue() )
												.sorted((a,b)->Double.compare(b,a))
												.collect(Collectors.toList());
			
			BiFunction<Integer,List<Transactions>,List<Transactions>> txnByYear = 
					(year,allTxns) ->	allTxns
											.stream()
											.filter(txn->getYear(new Date(txn.getTimeStamp()*1000))==year)
											.collect(Collectors.toList());
					
										
									
			System.out.println("SG Traders in ASC sort : "+tradersByCity.andThen(tradersSortByName).apply("Singapore",allTradersList));			
			System.out.println("Highest Transaction value (USD): "+first.compose(txnSortByValueDesc).apply(allTransactionsList));
			System.out.println("All Transactions in 2016 (USD) sort by value DESC: "+txnByYear.andThen(txnSortByValueDesc).apply(2016, allTransactionsList));			
			System.out.println("average value of transctions by Beijing traders (USD) : "+avgTransactionsByCity.apply("Beijing", allTransactionsList));
//			System.out.println("average value of transctions by Singapore traders (USD) : "+avgTransactionsByCity.apply("Singapore", allTransactionsList));
			System.out.println("\n\n\n");
			
						
		}
		
		
		public static int getYear(Date date){
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			return cal.get(Calendar.YEAR);
		}
		
		public static String getTransactionsJson(){		
			return getResponse("https://fvjkpkflnc.execute-api.us-east-1.amazonaws.com/prod/transactions");
		}
		
		public static String getResponse(String urlStr){
			System.out.println("GET "+urlStr);
			String response="";
			try{
			
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("x-api-key", "gaqcRZE4bd58gSAJH3XsLYBo1EvwIQo88IfYL1L5");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			
			System.out.println("Server Response ... Success\n");
			while ((output = br.readLine()) != null) {				
//				System.out.println(output);
				response+=output;
			}
			conn.disconnect();

		  } catch (MalformedURLException e) {
			e.printStackTrace();
		  } catch (IOException e) {
			e.printStackTrace();

		  }catch(Exception e){
			  e.printStackTrace();
		  }

		return response;
		}
		
	
		public static String getTradersJson(){
			return getResponse("https://fvjkpkflnc.execute-api.us-east-1.amazonaws.com/prod/traders");
		}
		
		
		public static ArrayList<Trader> getTradersList(){
			String tradersString=getTradersJson();
			System.out.println(tradersString);			

			Trader[] tradersA=new Gson().fromJson(tradersString, Trader[].class);			
		
			ArrayList<Trader> listOfTraders = new ArrayList<>(Arrays.asList(tradersA));
			
			return listOfTraders;
		}
	

	}


